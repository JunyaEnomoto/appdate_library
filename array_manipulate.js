
var Arr = {

    get_common_elements: function (arrs) {
        //arrs -> 二次元配列　
        //例えば配列を1つにまとめた  
        //var arrs = [arr1, arr2, arr3];ものを引数にし、arr 1~3 に共通する要素を配列で返す
        //参照）　https://keizokuma.com/js-array-all-exists/

        var resultArr = arrs.reduce(function (previousValue, currentValue, index, array) {
            //比較したい配列をreduceで結合して1つにまとめる
            return previousValue.concat(currentValue);

        }).filter(function (x, i, self) {
            //重複を削除
            return self.indexOf(x) === i;

        }).filter(function (val) {
            //比較したい配列(arr1,arr2,arr3)のすべてに含まれるもののみ抽出
            var flg = true;

            arrs.forEach(function (compArr) {
                flg = (compArr.indexOf(val) !== -1) && flg;
            });

            return flg;
        });
        return resultArr
    }

}