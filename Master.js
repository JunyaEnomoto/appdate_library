function Master(sample_ssurl, sample_sheet_name,header_volume = 1, header_row = 1) {
  class Master {
    //SSで空欄の箇所はそもそも定義されない
    //挙動としては[0][9]が空欄の時
    // if (Raw_data.values[0][9]=="") => true
    // if (Raw_data.values[0][9]==null) => false
    // if (Raw_data.values[0][9]) => false
    //なお空欄をnullに置き換えてsetValuesしてもスプレッドシートにはnullとは書かれない。s

    constructor(ssurl, sheet_name, header_volume, header_row) {
      this.url=ssurl;
      this.ss = SpreadsheetApp.openByUrl(ssurl);
      this.sheet = this.ss.getSheetByName(sheet_name);
      this.__values = this.sheet.getDataRange().getValues(); //row_valueでheaderも入ったもの。
      this.__set_header(header_volume, header_row);
      //todo本当はここは別々のメモリを使うようにしたい
      this.values = this.__values;
      for (var i = 0; i < this.header_volume; i++) {
        this.values.shift();
      }
      this.__set_last_arr();
      this.values_obj = get_values_as_obj(ssurl, sheet_name,header_volume, header_row);
    }
    __set_header(header_volume, header_row) {
      //header_row => headerがそのSSで何番目にあるか。普通は１行目
      //header_volume 普通は１行分。２行分あったら後で削除するshiftの回数を変えている
      //header は一次元配列で返すが、もし、SSのシートにそのまま当てはめたい時は
      //var header=[]
      //header.push(**.header)　のように２次元配列へ！
      this.header_volume = header_volume;
      this.header_row_index = header_row - 1;
      this.header = this.__values[this.header_row_index]; //->１次元配列
    }

    __set_last_arr() {
      //フォームの最後の列を取得する時に主に使いそう
      //シートの最終行を取得。
      var last_row = this.values.length;
      this.last_row_arr = this.values[last_row - 1];
    }
    header_search(key) {
      //header内にあるkey
      //一致した時のColumn_index ->Numberを返す。
      //合致しない時は undefinedが返る

      for (var hi = 0; hi < this.header.length; hi++) {
        if (this.header[hi] === key) {
          return hi;
        }
      }
    }
  }

  const master = new Master(sample_ssurl, sample_sheet_name,header_volume, header_row ); //->object
  return master;
}

function get_values_as_obj(url, sheetName,header_volume, header_row) {
  //参考 https://qiita.com/taichi0514/items/ee6dedff45f9d9e58ef4
  //Pythonでいうdict型で取得するイメージ
  //obj->list obj[0] ->Object

  var sheet = SpreadsheetApp.openByUrl(url).getSheetByName(sheetName);
  var rows = sheet.getDataRange().getValues();
  var keys = rows.splice(header_row-1, 1)[0];//ここで破壊されて,  rows からheaderが覗かれている
  
　for (var i =0;i<header_volume-1;i++){
    rows.shift()
  }

 return rows.map(function (row) {
    var obj = {};
    row.map(function (item, index) {
      obj[keys[index]] = item;
    });
    return obj;
  });
  
}


