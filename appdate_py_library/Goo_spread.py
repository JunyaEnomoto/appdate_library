
from google.auth import default
from google.oauth2.service_account import Credentials
from gspread_dataframe import set_with_dataframe
import gspread
import pandas as pd
import string
import math
import numpy as np
import re
import sys


class Goo_spread:
    def __init__(self,  url, state,
                 json_path, sheet_element="", header_volume=1, header_row=1):
        """_summary_
        注意：スプレッドシートのヘッダーが　同じものがあってはいけない。
        空欄もダメ。
        local で操作するときにSSの共有のところに
        id-668@hosoya-hd.iam.gserviceaccount.com
        を追加するのを忘れない！

        Parameters
        ----------
        url : _type_ str
            _description_
        state : local or colab
        json_path: "hosoya-hd-eb8d0ca90e2d.json" -> windows だとこういう感じ
                    "/Users/enomotojunya/Dropbox/hosoya/clinics_manipulate_library/hosoya-hd-eb8d0ca90e2d.json"
                    ->mac だとこんな感じ。
        sheet_element : _type_  int or str
            _description_: シートのindexかシート名を入れると
        header_volume : int, optional
            _description_, by default 1
        header_row : int, optional
            _description_, by default 1
        """
        self.url = url
        self.state = state

        # def set_creds

        if self.state == "local":
            # お決まりの文句
            # 2つのAPIを記述しないとリフレッシュトークンを3600秒毎に発行し続けなければならない
            scope = ['https://www.googleapis.com/auth/spreadsheets',
                     'https://www.googleapis.com/auth/drive']
            # ダウンロードしたjsonファイル名をクレデンシャル変数に設定。

            self.creds = Credentials.from_service_account_file(
                json_path, scopes=scope)

        elif self.state == "colab":
            # import pandas
            from google.colab import auth
            auth.authenticate_user()
            self.creds, _ = default()
        else:
            print("state errorのため終了")
            return

        if sheet_element != "":
            self.sheet = self.get_worksheet(sheet_element)
            print(type(self.sheet))
            self.df = self.get_values(header_volume, header_row)
        else:
            print("sheet_elementが指定されておりませんがよろしいでしょうか？つまりdfに値は収まっておりません。")
        self.sheets = []

    def get_worksheet(self, sheet_element) -> gspread.client.Client:
        # localだと　'gspread.worksheet.Worksheet'
        # colab だと class 'gspread.models.Worksheet' . つまり微妙に挙動が違う・・・
        # OAuth2の資格情報を使用してGoogle APIにログイン。
        gc = gspread.authorize(self.creds)
        try:
            if type(sheet_element) is int:
                sheet = gc.open_by_url(self.url).get_worksheet(sheet_element)
            elif type(sheet_element) is str:
                sheet = gc.open_by_url(self.url).worksheet(sheet_element)

            return sheet
        except:
            if self.state == "local":
                print("おそらくGCPのアドレスを共有してないことが最も原因として考えられます。")
            else:
                print("単純に共有権か書き込みなどの権限がないからでしょうか？")
            sys.exit()

    def set_sheets(self, sheet_elements):
        """_summary_

        Parameters
        ----------
        sheet_elements : _type_ list
            _description_ : listの中身は intかsheet_name いずれでも

        """
        [self.sheets.append(self.get_worksheet(i)) for i in sheet_elements]

    def get_values(self, header_volume, header_row):
        """
        get_all_recordsはdict 要素のlist
        get_all_valuesはlist （２次元）
        そして　ColabとLocal でpd.Dataframe(dict)の挙動が少し変わる。
        Colabだとヘッダーが空欄の時はその列は削除される
        Localだとヘッダーが重複すると（例え空白でも）削除される。
        というわけでvaluesを使用することにした。
        """

        df = pd.DataFrame(
            self.sheet.get_all_values())
        # 公式だとこちら　records  こちらの方がdataframeの使い方に合っている！index化されるのでSheetの形式次第か。

        if header_volume < header_row:
            print("header_volumeがheader_rowより小さいのはエラーです。")
            sys.exit()
        df.columns = list(df.loc[header_row-1, :])  # header分とindex 分

        for i in range(header_volume):
            df.drop(i, inplace=True)
        df.reset_index(inplace=True)
        df.drop('index', axis=1, inplace=True)
        return df

    def header_search(self, keyword, header_row=1):
        """
        header内にあるKeywordがあるかを検索して、なければメッセージ返す
        あればその列indexをかえす
        第１引数　keyword,第２引数　headerがある行、デフォルト１
        """

        header_list = self.sheet.row_values(header_row)

        if keyword not in header_list:
            print(keyword + "はheaderにありません")
            return

        return header_list.index(keyword)

    def update_row(self, first_row_num: int,
                   value_list: list):
        """
        worksheetと指定した行、そこに挿入したい価値(=value_list)を渡すとシートに上書きしてくれる。
        SSのセルの指定の仕方は　A1などのような形式なのでアルファベットの列が必要。
        範囲はfirst_row_num　からvalue_list の配列数いれる。
        今はまだ1列目からしか貼れない。。。

        列の途中から置換する、という概念はここにはないので注意
        1000列以上のものは一旦エラー返す仕様。必要時作り直す
        動かない時は
        pip install gspread
        を試してみてください。
        Goo_spread.Goo_spread.uppdate_row(12,value_list)
        呼び出す時はこのような形です。
        value_listは一次元配列でも２次元配列でもOK
        """

        def to_alphabets(value_list):
            # ABCDEFGHIJKLMNOPQRSTUVWXYZ 小文字 #->str
            alphabets = string.ascii_uppercase.lower()
            alphabets_list = []

            # エラーで終了
            if len(value_list) > 1000:
                print("1000列以上のものは現在は対応できません。")
                # return

            for i in range(26):
                alphabets_list.append(alphabets[i: i + 1])

            for j in range(math.ceil(len(value_list) / 26) - 1):
                for i in range(26):
                    alphabets_list.append(alphabets[j] + alphabets[i: i + 1])
            return alphabets_list

        alphabets_list = to_alphabets(value_list)
        start_col_index = 0

        # １行をまるまる挿入する
        last_col_index = len(value_list) - 1
        range_str = "".join(
            [alphabets_list[start_col_index], str(
                first_row_num), ":",
             alphabets_list[last_col_index], str(first_row_num)]
        )  # a10:k10

        cell_list = self.sheet.range(range_str)  # listなのにvalueという概念がある

        for i, cell in enumerate(cell_list):
            cell.value = value_list[i]

        self.sheet.update_cells(cell_list)

    def update_value(self, first_row_num: int, first_col_num: int,
                     value, must_be_header=False):
        """
        value は一次元配列〜２次元配列、Dataframe まで対応できるが
        Row が入った場合のみ、どの列から開始するかは選べない。
        """

        if type(value) == pd.core.frame.DataFrame:
            # "or type(value) == pandas.core.frame.DataFrame:
            value = value.astype("str")  # これをしないとエラーになるようだ。

        elif type(value) == list:
            dim = np.array(value).ndim
            if (dim == 1):
                print("Row が入った場合のみ、どの列から開始するかは選べない。")
                self.update_row(first_row_num,
                                value)
                sys.exit()
            elif (dim == 2):
                value = pd.DataFrame(value).astype("str")
                must_be_header = False

            else:
                print("貼り付けるlistの次元が多すぎます")
                sys.exit()

        else:
            print("valueのデータがたがあってません")

        def toAlpha(num):
            if num <= 26:
                return chr(64+num)
            elif num % 26 == 0:
                return toAlpha(num//26-1)+chr(90)
            else:
                return toAlpha(num//26)+chr(64+num % 26)

        start_cell = toAlpha(first_col_num)+str(first_row_num)

        def alpha_to_num(c):
            """
            # アルファベットから数字を返すラムダ式(A列～Z列まで)
             例：A→1、Z→26
            """
            return ord(c) - ord('A') + 1

        # 展開を開始するセルからA1セルの差分
        row_diff = first_row_num-1  # 3
        col_diff = alpha_to_num(toAlpha(first_col_num))-alpha_to_num('A')  # 2

        col_lastnum = len(value.columns)  # DataFrameの列数
        row_lastnum = len(value.index)   # DataFrameの行数

        if must_be_header:
            cell_list = self.sheet.range(
                start_cell+':'+toAlpha(col_lastnum+col_diff)
                + str(row_lastnum+1+row_diff))
        else:
            cell_list = self.sheet.range(
                start_cell+':'+toAlpha(col_lastnum+col_diff)
                + str(row_lastnum+1+row_diff-1))
        for cell in cell_list:
            if must_be_header:
                if cell.row == 1+row_diff:
                    val = value.columns[cell.col-(1+col_diff)]
                else:
                    val = value.iloc[cell.row -
                                     (2+row_diff)][cell.col-(1+col_diff)]
                cell.value = val
            else:
                val = value.iloc[cell.row+1 -
                                 (2+row_diff)][cell.col-(1+col_diff)]
                cell.value = val
        self.sheet.update_cells(cell_list)
