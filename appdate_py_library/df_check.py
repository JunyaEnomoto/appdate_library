# ここでは２つのdfを比較して、連結したり。
# id の重複がないか
# id が繋がった人の各要素が一致しているか。
# df1にあってdf2にない人。または逆。

import sys

sys.path.append("/content/drive/My Drive/appdate_py_library")

from google.colab import drive

drive.mount("/content/drive")

import Goo_spread as gs


class Config:
    """
    手入力するところ。
    df は現在はheader は１行目のみという縛りあり
    """

    def __init__(self):

        master_url = (
            "https://docs.google.com/spreadsheets/d/1aA4fLDbsfswFxPiG0wPNrtN-EPHu_dksuButKDUNpwE/edit#gid=0"
        )

        target_url = "https://docs.google.com/spreadsheets/d/1N8ozDpKdEkxmX14cnyBGBuH6l_5cqw0tRsth_A1pTVg/edit#gid=1991690958"
        self.master_df = gs.Goo_spread(master_url, 0).df
        self.target_df = gs.Goo_spread(target_url, 0).df
        # 調べたいdf の列
        self.target_id = "id"


class Db_check(Config):
    def __init__(self):
        super().__init__()
        # idの重複を確認します
        id_count = (df.duplicated(subset=self.target_id)).sum()  # ~をつけると重複していない行。
        print("".join(["重複したidは", str(id_count), "です"]))
        # その社員番号のひとが target,masterでどのように表示されているかを確認。

