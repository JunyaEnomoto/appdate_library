
function Cal_function(calendar_id) {

    class Cal_master {
        constructor(calendar_id) {
            this.calendar_id=calendar_id
            this.calendar = CalendarApp.getCalendarById(calendar_id); //産業医訪問
        }

        get_events_for_this_month() {
            var start_day = Moment.moment().startOf("month"); //-> e
            var last_day = Moment.moment().endOf("month"); //-> e   .format("YYYY/MM/DD HH:mm:ss")で認識できる形へ。
            var events = this.calendar.getEvents(start_day.toDate(), last_day.toDate());
            return events;
        }

        write_event_to_calendar(
            event_name, start, end, is_set = false, place = "", desc = ""
        ) {
            //これは指定したカレンダーに情報を書き込みます。
            //is_set 既にカレンダーにセットされていた場合True.デフォルトはfalse
            //start -> Date Object  (momentからは .toDate()でよい)
            //ex) var start =  Moment.moment("2021/5/23 12:00").toDate()
            //              =  new Date('July 20, 1969'));(多分)
            //momentで時間を入れなければ0時という設定となる

            if (is_set !== true) {
                var Cal = CalendarApp.getCalendarById(this.calendar_id);
                //指定のカレンダーIDへインベント登録
                Cal.createEvent(event_name, start, end, {location: place,description: desc});
                //カレンダー登録が終わったイベントのaddedへ「登録完了」を記入
                //sht.getRange(i, 7).setValue("登録完了");
            }
        }
    }

    const Cal = new Cal_master(calendar_id); //->object
    return Cal;
}
