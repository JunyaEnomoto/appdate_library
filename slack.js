
var Slack = {
//WebhookのURLは以下のURLから追加します　https://api.slack.com/apps/AN2BY3VLN/incoming-webhooks?success=1
    general:"https://hooks.slack.com/services/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",
    bot_relation_webhook_url: "https://hooks.slack.com/services/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",
    esa_url:"https://hooks.slack.com/services/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",
    stress_check_url:"https://hooks.slack.com/services/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",
    dipromacy_url:"https://hooks.slack.com/services/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",
    trade_url:"https://hooks.slack.com/services/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",
    attendance_management:"https://hooks.slack.com/services/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",
    mediation:"https://hooks.slack.com/services/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",
    leader:"https://hooks.slack.com/services/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",
    handy_v2:"https://hooks.slack.com/services/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",
    send_by_webhook: function (url, text, username = "", icon_emoji = "", title = "", disc = "", image_url = "") {
        //webhookで送る方法。
        //urlにChannel情報は含まれているので注意。

        var message = text;
        var jsonData =
        {
            "username": username,//"ユーザ名てすと",
            "icon_emoji": icon_emoji,//":dog:",
            "text": message,
            "attachments": [{
                "fields": [
                    {
                        "title": title,//"画像タイトル",
                        "value": disc,//"ここは画像の説明を記述できます。",
                    }
                ],
                "image_url": image_url//"https://specially198.com/wp-content/uploads/2020/10/logo-1-e1602060579286.png"
            }]
        };
        var payload = JSON.stringify(jsonData);

        var options =
        {
            "method": "post",
            "contentType": "application/json",
            "payload": payload
        };
        UrlFetchApp.fetch(url, options);
    }
}

