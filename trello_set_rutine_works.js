//read meを作る
//シートに更新日を入れたい

var Trello_obj = {
    __init__: function (
      api_key = "7106bcf8b4b265c3d6ad49a7d8d4d76c",
      api_token = "a6234c227cafc81bce3973a25890bc0f90e96779fb84dcc5c21319614c8a7d20"
    ) {
      // Trello API
      //        var trello_key   = "7106bcf8b4b265c3d6ad49a7d8d4d76c";//共通
      //        var trello_token = "a6234c227cafc81bce3973a25890bc0f90e96779fb84dcc5c21319614c8a7d20";//榎本共通
  
      this.url =
        "https://api.trello.com/1/cards/?key=" + api_key + "&token=" + api_token;
      return this;
    },
  
    //add_cardを作る
    add_card(list_id, task) {
      //card作成    list_idは指定したトレロボードの中のさらに詳細なリスト
      // task->Object
      var options = {
        method: "post",
        muteHttpExceptions: true,
        payload: {
          name: task.name_,
          desc: task.desc_,
          due: task.due_, //getDate(),OK   //"2016-05-15T08:00:00.000Z",OK
          idList: list_id,
          idMembers: task.idMembers_,
          idLabels: task.idLabels_,
          urlSource: "",
        }, //payload
      }; //options
      var response = UrlFetchApp.fetch(this.url, options);
    },
  }; //Trello
  
